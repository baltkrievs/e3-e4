#!/usr/bin/env python

import os

result = {}

f = open("txt/something.txt", "r")

for line in f:
    words = [w.strip(" \t\n\r,.();") for w in line.split(" ")]
    
    for word in words:
        slength = word.__len__()
        if slength > 0:
            if slength not in result:
                result[slength] = 1
            else:
                result[slength] += 1
f.close()

out = open("txt/result.txt", "w")

for k,v in result.iteritems():
    out.write(str(k) +" - "+ str(v) +"\n")

out.close()


