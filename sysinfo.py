# ./sysinfo.py
# -*- coding: utf-8 -*-
# PyXB bindings for NM:bf9525075a7cd06386684a3c22b125bdfdc70b82
# Generated 2014-05-22 23:26:45.555118 by PyXB version 1.2.3
# Namespace http://www.example.org/sysinfo

import pyxb
import pyxb.binding
import pyxb.binding.saxer
import io
import pyxb.utils.utility
import pyxb.utils.domutils
import sys

# Unique identifier for bindings created at the same time
_GenerationUID = pyxb.utils.utility.UniqueIdentifier('urn:uuid:64b70848-e1ef-11e3-82f3-dc85de91e9af')

# Version of PyXB used to generate the bindings
_PyXBVersion = '1.2.3'
# Generated bindings are not compatible across PyXB versions
if pyxb.__version__ != _PyXBVersion:
    raise pyxb.PyXBVersionError(_PyXBVersion)

# Import bindings for namespaces imported into schema
import pyxb.binding.datatypes

# NOTE: All namespace declarations are reserved within the binding
Namespace = pyxb.namespace.NamespaceForURI(u'http://www.example.org/sysinfo', create_if_missing=True)
Namespace.configureCategories(['typeBinding', 'elementBinding'])

def CreateFromDocument (xml_text, default_namespace=None, location_base=None):
    """Parse the given XML and use the document element to create a
    Python instance.

    @param xml_text An XML document.  This should be data (Python 2
    str or Python 3 bytes), or a text (Python 2 unicode or Python 3
    str) in the L{pyxb._InputEncoding} encoding.

    @keyword default_namespace The L{pyxb.Namespace} instance to use as the
    default namespace where there is no default namespace in scope.
    If unspecified or C{None}, the namespace of the module containing
    this function will be used.

    @keyword location_base: An object to be recorded as the base of all
    L{pyxb.utils.utility.Location} instances associated with events and
    objects handled by the parser.  You might pass the URI from which
    the document was obtained.
    """

    if pyxb.XMLStyle_saxer != pyxb._XMLStyle:
        dom = pyxb.utils.domutils.StringToDOM(xml_text)
        return CreateFromDOM(dom.documentElement)
    if default_namespace is None:
        default_namespace = Namespace.fallbackNamespace()
    saxer = pyxb.binding.saxer.make_parser(fallback_namespace=default_namespace, location_base=location_base)
    handler = saxer.getContentHandler()
    xmld = xml_text
    if isinstance(xmld, unicode):
        xmld = xmld.encode(pyxb._InputEncoding)
    saxer.parse(io.BytesIO(xmld))
    instance = handler.rootObject()
    return instance

def CreateFromDOM (node, default_namespace=None):
    """Create a Python instance from the given DOM node.
    The node tag must correspond to an element declaration in this module.

    @deprecated: Forcing use of DOM interface is unnecessary; use L{CreateFromDocument}."""
    if default_namespace is None:
        default_namespace = Namespace.fallbackNamespace()
    return pyxb.binding.basis.element.AnyCreateFromDOM(node, default_namespace)


# Complex type {http://www.example.org/sysinfo}SystemType with content type ELEMENT_ONLY
class SystemType (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/sysinfo}SystemType with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, u'SystemType')
    _XSDLocation = pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 5, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/sysinfo}Cpu uses Python identifier Cpu
    __Cpu = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, u'Cpu'), 'Cpu', '__httpwww_example_orgsysinfo_SystemType_httpwww_example_orgsysinfoCpu', False, pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 7, 6), )

    
    Cpu = property(__Cpu.value, __Cpu.set, None, None)

    
    # Element {http://www.example.org/sysinfo}Memory uses Python identifier Memory
    __Memory = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, u'Memory'), 'Memory', '__httpwww_example_orgsysinfo_SystemType_httpwww_example_orgsysinfoMemory', False, pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 8, 6), )

    
    Memory = property(__Memory.value, __Memory.set, None, None)

    
    # Element {http://www.example.org/sysinfo}Devices uses Python identifier Devices
    __Devices = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, u'Devices'), 'Devices', '__httpwww_example_orgsysinfo_SystemType_httpwww_example_orgsysinfoDevices', False, pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 9, 6), )

    
    Devices = property(__Devices.value, __Devices.set, None, None)

    
    # Attribute system uses Python identifier system
    __system = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, u'system'), 'system', '__httpwww_example_orgsysinfo_SystemType_system', pyxb.binding.datatypes.string)
    __system._DeclarationLocation = pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 13, 5)
    __system._UseLocation = pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 13, 5)
    
    system = property(__system.value, __system.set, None, None)

    
    # Attribute node uses Python identifier node
    __node = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, u'node'), 'node', '__httpwww_example_orgsysinfo_SystemType_node', pyxb.binding.datatypes.string)
    __node._DeclarationLocation = pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 14, 5)
    __node._UseLocation = pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 14, 5)
    
    node = property(__node.value, __node.set, None, None)

    
    # Attribute release uses Python identifier release
    __release = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, u'release'), 'release', '__httpwww_example_orgsysinfo_SystemType_release', pyxb.binding.datatypes.string)
    __release._DeclarationLocation = pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 15, 5)
    __release._UseLocation = pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 15, 5)
    
    release = property(__release.value, __release.set, None, None)

    
    # Attribute version uses Python identifier version
    __version = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, u'version'), 'version', '__httpwww_example_orgsysinfo_SystemType_version', pyxb.binding.datatypes.string)
    __version._DeclarationLocation = pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 16, 5)
    __version._UseLocation = pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 16, 5)
    
    version = property(__version.value, __version.set, None, None)

    
    # Attribute machine uses Python identifier machine
    __machine = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, u'machine'), 'machine', '__httpwww_example_orgsysinfo_SystemType_machine', pyxb.binding.datatypes.string)
    __machine._DeclarationLocation = pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 17, 5)
    __machine._UseLocation = pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 17, 5)
    
    machine = property(__machine.value, __machine.set, None, None)

    _ElementMap.update({
        __Cpu.name() : __Cpu,
        __Memory.name() : __Memory,
        __Devices.name() : __Devices
    })
    _AttributeMap.update({
        __system.name() : __system,
        __node.name() : __node,
        __release.name() : __release,
        __version.name() : __version,
        __machine.name() : __machine
    })
Namespace.addCategoryObject('typeBinding', u'SystemType', SystemType)


# Complex type {http://www.example.org/sysinfo}Cpu with content type ELEMENT_ONLY
class Cpu (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/sysinfo}Cpu with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, u'Cpu')
    _XSDLocation = pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 21, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/sysinfo}cores uses Python identifier cores
    __cores = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, u'cores'), 'cores', '__httpwww_example_orgsysinfo_Cpu_httpwww_example_orgsysinfocores', False, pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 23, 6), )

    
    cores = property(__cores.value, __cores.set, None, None)

    
    # Element {http://www.example.org/sysinfo}usage uses Python identifier usage
    __usage = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, u'usage'), 'usage', '__httpwww_example_orgsysinfo_Cpu_httpwww_example_orgsysinfousage', False, pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 24, 6), )

    
    usage = property(__usage.value, __usage.set, None, None)

    _ElementMap.update({
        __cores.name() : __cores,
        __usage.name() : __usage
    })
    _AttributeMap.update({
        
    })
Namespace.addCategoryObject('typeBinding', u'Cpu', Cpu)


# Complex type {http://www.example.org/sysinfo}Memory with content type ELEMENT_ONLY
class Memory (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/sysinfo}Memory with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, u'Memory')
    _XSDLocation = pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 29, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/sysinfo}physical uses Python identifier physical
    __physical = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, u'physical'), 'physical', '__httpwww_example_orgsysinfo_Memory_httpwww_example_orgsysinfophysical', False, pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 31, 6), )

    
    physical = property(__physical.value, __physical.set, None, None)

    
    # Element {http://www.example.org/sysinfo}swap uses Python identifier swap
    __swap = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, u'swap'), 'swap', '__httpwww_example_orgsysinfo_Memory_httpwww_example_orgsysinfoswap', False, pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 32, 6), )

    
    swap = property(__swap.value, __swap.set, None, None)

    _ElementMap.update({
        __physical.name() : __physical,
        __swap.name() : __swap
    })
    _AttributeMap.update({
        
    })
Namespace.addCategoryObject('typeBinding', u'Memory', Memory)


# Complex type {http://www.example.org/sysinfo}Devices with content type ELEMENT_ONLY
class Devices (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/sysinfo}Devices with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, u'Devices')
    _XSDLocation = pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 37, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/sysinfo}device uses Python identifier device
    __device = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, u'device'), 'device', '__httpwww_example_orgsysinfo_Devices_httpwww_example_orgsysinfodevice', True, pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 40, 6), )

    
    device = property(__device.value, __device.set, None, None)

    _ElementMap.update({
        __device.name() : __device
    })
    _AttributeMap.update({
        
    })
Namespace.addCategoryObject('typeBinding', u'Devices', Devices)


# Complex type {http://www.example.org/sysinfo}MemoryType with content type ELEMENT_ONLY
class MemoryType (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/sysinfo}MemoryType with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, u'MemoryType')
    _XSDLocation = pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 48, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/sysinfo}total uses Python identifier total
    __total = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, u'total'), 'total', '__httpwww_example_orgsysinfo_MemoryType_httpwww_example_orgsysinfototal', False, pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 50, 6), )

    
    total = property(__total.value, __total.set, None, None)

    
    # Element {http://www.example.org/sysinfo}percent uses Python identifier percent
    __percent = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, u'percent'), 'percent', '__httpwww_example_orgsysinfo_MemoryType_httpwww_example_orgsysinfopercent', False, pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 51, 6), )

    
    percent = property(__percent.value, __percent.set, None, None)

    
    # Element {http://www.example.org/sysinfo}available uses Python identifier available
    __available = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, u'available'), 'available', '__httpwww_example_orgsysinfo_MemoryType_httpwww_example_orgsysinfoavailable', False, pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 52, 6), )

    
    available = property(__available.value, __available.set, None, None)

    
    # Element {http://www.example.org/sysinfo}used uses Python identifier used
    __used = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, u'used'), 'used', '__httpwww_example_orgsysinfo_MemoryType_httpwww_example_orgsysinfoused', False, pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 53, 6), )

    
    used = property(__used.value, __used.set, None, None)

    _ElementMap.update({
        __total.name() : __total,
        __percent.name() : __percent,
        __available.name() : __available,
        __used.name() : __used
    })
    _AttributeMap.update({
        
    })
Namespace.addCategoryObject('typeBinding', u'MemoryType', MemoryType)


# Complex type {http://www.example.org/sysinfo}DeviceType with content type ELEMENT_ONLY
class DeviceType (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/sysinfo}DeviceType with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, u'DeviceType')
    _XSDLocation = pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 58, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/sysinfo}partition uses Python identifier partition
    __partition = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, u'partition'), 'partition', '__httpwww_example_orgsysinfo_DeviceType_httpwww_example_orgsysinfopartition', False, pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 60, 6), )

    
    partition = property(__partition.value, __partition.set, None, None)

    
    # Element {http://www.example.org/sysinfo}total uses Python identifier total
    __total = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, u'total'), 'total', '__httpwww_example_orgsysinfo_DeviceType_httpwww_example_orgsysinfototal', False, pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 61, 6), )

    
    total = property(__total.value, __total.set, None, None)

    
    # Element {http://www.example.org/sysinfo}percent uses Python identifier percent
    __percent = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, u'percent'), 'percent', '__httpwww_example_orgsysinfo_DeviceType_httpwww_example_orgsysinfopercent', False, pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 62, 6), )

    
    percent = property(__percent.value, __percent.set, None, None)

    
    # Element {http://www.example.org/sysinfo}used uses Python identifier used
    __used = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, u'used'), 'used', '__httpwww_example_orgsysinfo_DeviceType_httpwww_example_orgsysinfoused', False, pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 63, 6), )

    
    used = property(__used.value, __used.set, None, None)

    
    # Element {http://www.example.org/sysinfo}free uses Python identifier free
    __free = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, u'free'), 'free', '__httpwww_example_orgsysinfo_DeviceType_httpwww_example_orgsysinfofree', False, pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 64, 6), )

    
    free = property(__free.value, __free.set, None, None)

    _ElementMap.update({
        __partition.name() : __partition,
        __total.name() : __total,
        __percent.name() : __percent,
        __used.name() : __used,
        __free.name() : __free
    })
    _AttributeMap.update({
        
    })
Namespace.addCategoryObject('typeBinding', u'DeviceType', DeviceType)


# Complex type {http://www.example.org/sysinfo}CpuCoresType with content type ELEMENT_ONLY
class CpuCoresType (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/sysinfo}CpuCoresType with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, u'CpuCoresType')
    _XSDLocation = pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 68, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/sysinfo}physical uses Python identifier physical
    __physical = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, u'physical'), 'physical', '__httpwww_example_orgsysinfo_CpuCoresType_httpwww_example_orgsysinfophysical', False, pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 70, 6), )

    
    physical = property(__physical.value, __physical.set, None, None)

    
    # Element {http://www.example.org/sysinfo}logical uses Python identifier logical
    __logical = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, u'logical'), 'logical', '__httpwww_example_orgsysinfo_CpuCoresType_httpwww_example_orgsysinfological', False, pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 71, 6), )

    
    logical = property(__logical.value, __logical.set, None, None)

    _ElementMap.update({
        __physical.name() : __physical,
        __logical.name() : __logical
    })
    _AttributeMap.update({
        
    })
Namespace.addCategoryObject('typeBinding', u'CpuCoresType', CpuCoresType)


SystemInfo = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, u'SystemInfo'), SystemType, location=pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 46, 4))
Namespace.addCategoryObject('elementBinding', SystemInfo.name().localName(), SystemInfo)



SystemType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, u'Cpu'), Cpu, scope=SystemType, location=pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 7, 6)))

SystemType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, u'Memory'), Memory, scope=SystemType, location=pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 8, 6)))

SystemType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, u'Devices'), Devices, scope=SystemType, location=pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 9, 6)))

def _BuildAutomaton ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton
    del _BuildAutomaton
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0L, max=1L, metadata=pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 9, 6))
    counters.add(cc_0)
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(SystemType._UseForTag(pyxb.namespace.ExpandedName(Namespace, u'Cpu')), pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 7, 6))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(SystemType._UseForTag(pyxb.namespace.ExpandedName(Namespace, u'Memory')), pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 8, 6))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(SystemType._UseForTag(pyxb.namespace.ExpandedName(Namespace, u'Devices')), pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 9, 6))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_2._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
SystemType._Automaton = _BuildAutomaton()




Cpu._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, u'cores'), CpuCoresType, scope=Cpu, location=pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 23, 6)))

Cpu._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, u'usage'), pyxb.binding.datatypes.float, scope=Cpu, location=pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 24, 6)))

def _BuildAutomaton_ ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_
    del _BuildAutomaton_
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(Cpu._UseForTag(pyxb.namespace.ExpandedName(Namespace, u'cores')), pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 23, 6))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(Cpu._UseForTag(pyxb.namespace.ExpandedName(Namespace, u'usage')), pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 24, 6))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
Cpu._Automaton = _BuildAutomaton_()




Memory._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, u'physical'), MemoryType, scope=Memory, location=pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 31, 6)))

Memory._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, u'swap'), MemoryType, scope=Memory, location=pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 32, 6)))

def _BuildAutomaton_2 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_2
    del _BuildAutomaton_2
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(Memory._UseForTag(pyxb.namespace.ExpandedName(Namespace, u'physical')), pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 31, 6))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(Memory._UseForTag(pyxb.namespace.ExpandedName(Namespace, u'swap')), pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 32, 6))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
Memory._Automaton = _BuildAutomaton_2()




Devices._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, u'device'), DeviceType, scope=Devices, location=pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 40, 6)))

def _BuildAutomaton_3 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_3
    del _BuildAutomaton_3
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0L, max=None, metadata=pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 40, 6))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(Devices._UseForTag(pyxb.namespace.ExpandedName(Namespace, u'device')), pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 40, 6))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
Devices._Automaton = _BuildAutomaton_3()




MemoryType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, u'total'), pyxb.binding.datatypes.long, scope=MemoryType, location=pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 50, 6)))

MemoryType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, u'percent'), pyxb.binding.datatypes.float, scope=MemoryType, location=pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 51, 6)))

MemoryType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, u'available'), pyxb.binding.datatypes.long, scope=MemoryType, location=pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 52, 6)))

MemoryType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, u'used'), pyxb.binding.datatypes.long, scope=MemoryType, location=pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 53, 6)))

def _BuildAutomaton_4 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_4
    del _BuildAutomaton_4
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(MemoryType._UseForTag(pyxb.namespace.ExpandedName(Namespace, u'total')), pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 50, 6))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(MemoryType._UseForTag(pyxb.namespace.ExpandedName(Namespace, u'percent')), pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 51, 6))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(MemoryType._UseForTag(pyxb.namespace.ExpandedName(Namespace, u'available')), pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 52, 6))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(MemoryType._UseForTag(pyxb.namespace.ExpandedName(Namespace, u'used')), pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 53, 6))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
         ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    st_3._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
MemoryType._Automaton = _BuildAutomaton_4()




DeviceType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, u'partition'), pyxb.binding.datatypes.string, scope=DeviceType, location=pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 60, 6)))

DeviceType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, u'total'), pyxb.binding.datatypes.long, scope=DeviceType, location=pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 61, 6)))

DeviceType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, u'percent'), pyxb.binding.datatypes.float, scope=DeviceType, location=pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 62, 6)))

DeviceType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, u'used'), pyxb.binding.datatypes.long, scope=DeviceType, location=pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 63, 6)))

DeviceType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, u'free'), pyxb.binding.datatypes.long, scope=DeviceType, location=pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 64, 6)))

def _BuildAutomaton_5 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_5
    del _BuildAutomaton_5
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(DeviceType._UseForTag(pyxb.namespace.ExpandedName(Namespace, u'partition')), pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 60, 6))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(DeviceType._UseForTag(pyxb.namespace.ExpandedName(Namespace, u'total')), pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 61, 6))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(DeviceType._UseForTag(pyxb.namespace.ExpandedName(Namespace, u'percent')), pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 62, 6))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(DeviceType._UseForTag(pyxb.namespace.ExpandedName(Namespace, u'used')), pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 63, 6))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(DeviceType._UseForTag(pyxb.namespace.ExpandedName(Namespace, u'free')), pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 64, 6))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
         ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
         ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    st_4._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
DeviceType._Automaton = _BuildAutomaton_5()




CpuCoresType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, u'physical'), pyxb.binding.datatypes.int, scope=CpuCoresType, location=pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 70, 6)))

CpuCoresType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, u'logical'), pyxb.binding.datatypes.int, scope=CpuCoresType, location=pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 71, 6)))

def _BuildAutomaton_6 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_6
    del _BuildAutomaton_6
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CpuCoresType._UseForTag(pyxb.namespace.ExpandedName(Namespace, u'physical')), pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 70, 6))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CpuCoresType._UseForTag(pyxb.namespace.ExpandedName(Namespace, u'logical')), pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/sysinfo.xsd', 71, 6))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CpuCoresType._Automaton = _BuildAutomaton_6()

