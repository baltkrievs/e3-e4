#!/usr/bin/env python

from prime import prime
import unittest


class PrimeTestCase(unittest.TestCase):
    def test_non_digit_parameter(self):
        """Tests alphabetic character is passed as a parameter"""
        self.assertTrue(prime("test") == [], "List is not empty")


    def test_list_length(self):
        self.assertTrue(len(prime(10)) == 10, "function returns wrong quantity of prime numbers")


    def test_first_value(self):
        """Tests first value in a list"""
        first_prime = prime(1)[0]
        self.assertTrue(first_prime == 2, "First value is not equal 2")


    def test_500_value(self):
        """Tests 500th value in a list"""
        self.assertTrue(prime(500)[499] == 3571, "500th value is not equal 3571")
