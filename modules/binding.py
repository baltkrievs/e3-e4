# ./binding.py
# -*- coding: utf-8 -*-
# PyXB bindings for NM:e92452c8d3e28a9e27abfc9994d2007779e7f4c9
# Generated 2014-05-20 23:15:13.956228 by PyXB version 1.2.3
# Namespace AbsentNamespace0

import pyxb
import pyxb.binding
import pyxb.binding.saxer
import io
import pyxb.utils.utility
import pyxb.utils.domutils
import sys

# Unique identifier for bindings created at the same time
_GenerationUID = pyxb.utils.utility.UniqueIdentifier('urn:uuid:73b27390-e05b-11e3-b7cd-dc85de91e9af')

# Version of PyXB used to generate the bindings
_PyXBVersion = '1.2.3'
# Generated bindings are not compatible across PyXB versions
if pyxb.__version__ != _PyXBVersion:
    raise pyxb.PyXBVersionError(_PyXBVersion)

# Import bindings for namespaces imported into schema
import pyxb.binding.datatypes

# NOTE: All namespace declarations are reserved within the binding
Namespace = pyxb.namespace.CreateAbsentNamespace()
Namespace.configureCategories(['typeBinding', 'elementBinding'])

def CreateFromDocument (xml_text, default_namespace=None, location_base=None):
    """Parse the given XML and use the document element to create a
    Python instance.

    @param xml_text An XML document.  This should be data (Python 2
    str or Python 3 bytes), or a text (Python 2 unicode or Python 3
    str) in the L{pyxb._InputEncoding} encoding.

    @keyword default_namespace The L{pyxb.Namespace} instance to use as the
    default namespace where there is no default namespace in scope.
    If unspecified or C{None}, the namespace of the module containing
    this function will be used.

    @keyword location_base: An object to be recorded as the base of all
    L{pyxb.utils.utility.Location} instances associated with events and
    objects handled by the parser.  You might pass the URI from which
    the document was obtained.
    """

    if pyxb.XMLStyle_saxer != pyxb._XMLStyle:
        dom = pyxb.utils.domutils.StringToDOM(xml_text)
        return CreateFromDOM(dom.documentElement)
    if default_namespace is None:
        default_namespace = Namespace.fallbackNamespace()
    saxer = pyxb.binding.saxer.make_parser(fallback_namespace=default_namespace, location_base=location_base)
    handler = saxer.getContentHandler()
    xmld = xml_text
    if isinstance(xmld, unicode):
        xmld = xmld.encode(pyxb._InputEncoding)
    saxer.parse(io.BytesIO(xmld))
    instance = handler.rootObject()
    return instance

def CreateFromDOM (node, default_namespace=None):
    """Create a Python instance from the given DOM node.
    The node tag must correspond to an element declaration in this module.

    @deprecated: Forcing use of DOM interface is unnecessary; use L{CreateFromDocument}."""
    if default_namespace is None:
        default_namespace = Namespace.fallbackNamespace()
    return pyxb.binding.basis.element.AnyCreateFromDOM(node, default_namespace)


# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 4, 2)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element orderperson uses Python identifier orderperson
    __orderperson = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, u'orderperson'), 'orderperson', '__AbsentNamespace0_CTD_ANON_orderperson', False, pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 6, 6), )

    
    orderperson = property(__orderperson.value, __orderperson.set, None, None)

    
    # Element shipto uses Python identifier shipto
    __shipto = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, u'shipto'), 'shipto', '__AbsentNamespace0_CTD_ANON_shipto', False, pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 7, 6), )

    
    shipto = property(__shipto.value, __shipto.set, None, None)

    
    # Element item uses Python identifier item
    __item = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, u'item'), 'item', '__AbsentNamespace0_CTD_ANON_item', True, pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 17, 6), )

    
    item = property(__item.value, __item.set, None, None)

    
    # Attribute orderid uses Python identifier orderid
    __orderid = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, u'orderid'), 'orderid', '__AbsentNamespace0_CTD_ANON_orderid', pyxb.binding.datatypes.string, required=True)
    __orderid._DeclarationLocation = pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 28, 4)
    __orderid._UseLocation = pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 28, 4)
    
    orderid = property(__orderid.value, __orderid.set, None, None)

    _ElementMap.update({
        __orderperson.name() : __orderperson,
        __shipto.name() : __shipto,
        __item.name() : __item
    })
    _AttributeMap.update({
        __orderid.name() : __orderid
    })



# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_ (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 8, 8)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element name uses Python identifier name
    __name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, u'name'), 'name', '__AbsentNamespace0_CTD_ANON__name', False, pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 10, 12), )

    
    name = property(__name.value, __name.set, None, None)

    
    # Element address uses Python identifier address
    __address = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, u'address'), 'address', '__AbsentNamespace0_CTD_ANON__address', False, pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 11, 12), )

    
    address = property(__address.value, __address.set, None, None)

    
    # Element city uses Python identifier city
    __city = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, u'city'), 'city', '__AbsentNamespace0_CTD_ANON__city', False, pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 12, 12), )

    
    city = property(__city.value, __city.set, None, None)

    
    # Element country uses Python identifier country
    __country = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, u'country'), 'country', '__AbsentNamespace0_CTD_ANON__country', False, pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 13, 12), )

    
    country = property(__country.value, __country.set, None, None)

    _ElementMap.update({
        __name.name() : __name,
        __address.name() : __address,
        __city.name() : __city,
        __country.name() : __country
    })
    _AttributeMap.update({
        
    })



# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_2 (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 18, 8)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element title uses Python identifier title
    __title = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, u'title'), 'title', '__AbsentNamespace0_CTD_ANON_2_title', False, pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 20, 12), )

    
    title = property(__title.value, __title.set, None, None)

    
    # Element note uses Python identifier note
    __note = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, u'note'), 'note', '__AbsentNamespace0_CTD_ANON_2_note', False, pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 21, 12), )

    
    note = property(__note.value, __note.set, None, None)

    
    # Element quantity uses Python identifier quantity
    __quantity = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, u'quantity'), 'quantity', '__AbsentNamespace0_CTD_ANON_2_quantity', False, pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 22, 5), )

    
    quantity = property(__quantity.value, __quantity.set, None, None)

    
    # Element price uses Python identifier price
    __price = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, u'price'), 'price', '__AbsentNamespace0_CTD_ANON_2_price', False, pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 23, 7), )

    
    price = property(__price.value, __price.set, None, None)

    _ElementMap.update({
        __title.name() : __title,
        __note.name() : __note,
        __quantity.name() : __quantity,
        __price.name() : __price
    })
    _AttributeMap.update({
        
    })



shiporder = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, u'shiporder'), CTD_ANON, location=pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 3, 0))
Namespace.addCategoryObject('elementBinding', shiporder.name().localName(), shiporder)



CTD_ANON._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, u'orderperson'), pyxb.binding.datatypes.string, scope=CTD_ANON, location=pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 6, 6)))

CTD_ANON._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, u'shipto'), CTD_ANON_, scope=CTD_ANON, location=pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 7, 6)))

CTD_ANON._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, u'item'), CTD_ANON_2, scope=CTD_ANON, location=pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 17, 6)))

def _BuildAutomaton ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton
    del _BuildAutomaton
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON._UseForTag(pyxb.namespace.ExpandedName(None, u'orderperson')), pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 6, 6))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON._UseForTag(pyxb.namespace.ExpandedName(None, u'shipto')), pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 7, 6))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CTD_ANON._UseForTag(pyxb.namespace.ExpandedName(None, u'item')), pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 17, 6))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_2._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CTD_ANON._Automaton = _BuildAutomaton()




CTD_ANON_._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, u'name'), pyxb.binding.datatypes.string, scope=CTD_ANON_, location=pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 10, 12)))

CTD_ANON_._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, u'address'), pyxb.binding.datatypes.string, scope=CTD_ANON_, location=pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 11, 12)))

CTD_ANON_._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, u'city'), pyxb.binding.datatypes.string, scope=CTD_ANON_, location=pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 12, 12)))

CTD_ANON_._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, u'country'), pyxb.binding.datatypes.string, scope=CTD_ANON_, location=pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 13, 12)))

def _BuildAutomaton_ ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_
    del _BuildAutomaton_
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_._UseForTag(pyxb.namespace.ExpandedName(None, u'name')), pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 10, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_._UseForTag(pyxb.namespace.ExpandedName(None, u'address')), pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 11, 12))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_._UseForTag(pyxb.namespace.ExpandedName(None, u'city')), pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 12, 12))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_._UseForTag(pyxb.namespace.ExpandedName(None, u'country')), pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 13, 12))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
         ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    st_3._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CTD_ANON_._Automaton = _BuildAutomaton_()




CTD_ANON_2._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, u'title'), pyxb.binding.datatypes.string, scope=CTD_ANON_2, location=pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 20, 12)))

CTD_ANON_2._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, u'note'), pyxb.binding.datatypes.string, scope=CTD_ANON_2, location=pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 21, 12)))

CTD_ANON_2._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, u'quantity'), pyxb.binding.datatypes.positiveInteger, scope=CTD_ANON_2, location=pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 22, 5)))

CTD_ANON_2._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, u'price'), pyxb.binding.datatypes.decimal, scope=CTD_ANON_2, location=pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 23, 7)))

def _BuildAutomaton_2 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_2
    del _BuildAutomaton_2
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0L, max=1, metadata=pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 21, 12))
    counters.add(cc_0)
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_2._UseForTag(pyxb.namespace.ExpandedName(None, u'title')), pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 20, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_2._UseForTag(pyxb.namespace.ExpandedName(None, u'note')), pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 21, 12))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_2._UseForTag(pyxb.namespace.ExpandedName(None, u'quantity')), pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 22, 5))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_2._UseForTag(pyxb.namespace.ExpandedName(None, u'price')), pyxb.utils.utility.Location('/home/misha/python/E3-E4/xsd/schema.xsd', 23, 7))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    transitions.append(fac.Transition(st_2, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
         ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    st_3._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CTD_ANON_2._Automaton = _BuildAutomaton_2()

