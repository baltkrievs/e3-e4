#!/usr/bin/env python

import binding
import pyxb

shiporder = binding.shiporder()
shiporder.shipto = pyxb.BIND('Name', 'address', 'city', 'country')
shiporder.item.append(pyxb.BIND('Title', 'note', 2, 12.5))
#item = schema.item()

shiporder.orderperson = 'John Doe'

shiporder.name = 'John'
shiporder.address = 'Street'
shiporder.city = 'Minsk'
shiporder.country = 'BY'

shiporder.orderid = 1234
shiporder.item.title = 'title'
shiporder.item.quantity = 1
shiporder.item.price = 12.5


xml = shiporder.toxml('utf-8')
print(xml)
