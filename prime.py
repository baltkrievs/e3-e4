#!/usr/bin/env python

import math

#isPrime() function checks is number is prime
def isPrime(number):
    if number == 1: return False                      # 1 is not prime number
    isPrime = True
    for n in range(2, int(math.sqrt(number)) + 1):
        if number % n == 0:
            return False

    return True


#prime() function returns desired quantity (passed as a parameter) of prime numbers
def prime(count):

    if type(count) is not int: return []

    prime_list = []
    current_num = 1

    while len(prime_list) < count:
        if current_num < 3:
            current_num += 1
        else:
            current_num += 2

        if isPrime(current_num):
            prime_list.append(current_num)

    return prime_list


def main():

    print("Enter quantity of prime numbers:")
    quantity = ""
    try:
        quantity = int(input())
    except:
        print("Enter numeric value")

    prime_list = prime(quantity)
    print " ".join(str(n) for n in prime_list)


if __name__ == "__main__": main()

