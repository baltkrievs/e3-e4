#!/usr/bin/env python

import platform
import psutil
import sysinfo
import pyxb
import lxml.etree as etree


def prettyxml(xml):
    xml = etree.fromstring(xml)
    return etree.tostring(xml, pretty_print=True)

#map attribures
system = platform.system()
node = platform.node()
release = platform.release()
version = platform.version()
machine = platform.machine()

#Inflate complex types
si = sysinfo.SystemInfo()
cpucores = sysinfo.CpuCoresType()
memory = sysinfo.MemoryType()
swapmemory = sysinfo.MemoryType()


cpuusage = psutil.cpu_percent(interval=1, percpu=False)

if psutil.cpu_count(logical=True) == None:
    cpucores.logical = 1
else:
    cpucores.logical = psutil.cpu_count(logical=True)

if psutil.cpu_count(logical=False) == None:
    cpucores.physical = 1
else:
    cpucores.physical = psutil.cpu_count(logical=False)

pmem = psutil.virtual_memory()
swap = psutil.swap_memory()

#map memory object
memory.total = pmem.total
memory.percent = pmem.percent
memory.available = pmem.available
memory.used = pmem.used

#map swap object
swapmemory.total = swap.total
swapmemory.percent = swap.percent
swapmemory.available = swap.free
swapmemory.used = swap.used

#build root element
si.Cpu = pyxb.BIND(cpucores, cpuusage)
si.Memory = pyxb.BIND(memory, swapmemory)
si.Devices = pyxb.BIND()
si.system = system
si.node = node
si.release = release
si.version = version
si.machine = machine


#map devices
for part in psutil.disk_partitions(all=False):
    dev = psutil.disk_usage(part.mountpoint)
    partition = part.device
    total = dev.total
    percent = dev.percent
    used = dev.used
    free = dev.free
    si.Devices.device.append(pyxb.BIND(partition, total, percent, used, free))

#build xml
xml = si.toxml("UTF-8")

print(prettyxml(xml))

#save xml on disk
try:
    f = open("xml/systeminfo.xml", "w")
    f.write(prettyxml(xml))

except IOError:
    print "Error: cant\'t open file"

finally:
    f.close()
    
